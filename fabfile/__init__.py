from fabric.api import task, local, run, put, cd, lcd
import os
import specs
import features
from fabric.context_managers import settings
import glob
from pypirss import helpers
from slimit import minify

@task
def test():
    specs.run()
    features.run()

@task
def cov():
    specs.cov("-p")
    features.cov("-p")
    local('coverage combine')
    local('coverage report')
    local('coverage html')

@task
def lint():
    local("rm -rf pylint")
    local("pylint --files-output=y -i y --comment=y --ignore=tests pypirss || true ")
    local("mkdir -p pylint")
    local("mv pylint_*.txt pylint/")

def _process_coffeescript():
    curdir = os.getcwd()
    os.chdir('pypirss')
    for filename in glob.glob("templates/*.html"):
        def tmp():
            with open(filename) as f:
                content = f.read()
            return content
        result = helpers.process_coffeescript(tmp)
        with open(filename, 'w') as f:
            f.write(result)
    os.chdir(curdir)

def _minify_js():
    for filename in ["pypirss/static/pypirss.js"]:
        print "trying to compress", filename
        mangled_filename = filename.replace('.js', '.mangled.js')
        with open(filename) as f:
            content = f.read()
        result = minify(content, mangle=True)
        with open(mangled_filename, 'w') as f:
            f.write(result)
        if os.path.getsize(mangled_filename) < os.path.getatime(filename):
            os.remove(filename)
            os.rename(mangled_filename, filename)
        else:
            os.remove(mangled_filename)
        

@task
def build():
    _process_coffeescript()
    _minify_js()
    with settings(warn_only=True):
        local("rm dist/*.tar.gz")
    local("python setup.py sdist")

@task
def serve():
    if not os.path.exists("static"):
        local("ln -s pypirss/static .")
    try:
        local("python pypirss/application.py")
    finally:
        if os.path.exists("static"):
            local("rm static")

@task
def deploy():
    package = [f for f in os.listdir('dist') if f.startswith('PypiRSS')][0]
    package_path = os.path.join('~/pypirss.aerdhyl.eu', package)
    put(os.path.join('dist', package), package_path)
    with cd('~/pypirss.aerdhyl.eu'):
        run('source ~/env/pypirss.aerdhyl.eu/bin/activate '
            '&& pip uninstall PypiRSS'.format(package))
        run('source ~/env/pypirss.aerdhyl.eu/bin/activate '
            '&& pip install {0}'.format(package))
        run("rm -rf public/static")
        run('cp -r ~/env/pypirss.aerdhyl.eu/lib/python2.7/site-packages/pypirss/static '
            'public/')
        run('touch passenger_wsgi.py')