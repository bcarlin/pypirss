from fabric.api import task, local, run, put, cd
import os

__all__ = ['run', 'cov']

@task(default=True)
def run():
    local("specloud")

@task
def cov(add_arg=""):
    base_dir = os.environ.get('VIRTUAL_ENV', os.path.join(os.getcwd(), '..'))
    nose_path = os.path.join(base_dir, 'bin', 'nosetests')
    nose_path = os.path.normpath(nose_path)
    local("coverage run {add_arg} {nose_path} "
          "-i '^(it|ensure|must|should|specs?|examples?|deve|Describe)' "
          "-i '(specs?(.py)?|examples?(.py)?)$' pypirss || true".format(
                nose_path=nose_path, add_arg=add_arg))
    if os.path.exists(".coverage"):
        local("coverage html")


    
