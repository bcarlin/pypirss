from fabric.api import task, local, run, put, cd
import os


@task(default=True)
def run():
    local('behave')

@task
def cov(add_arg=""):
    base_dir = os.environ.get('VIRTUAL_ENV', os.path.join(os.getcwd(), '..'))
    behave_path = os.path.join(base_dir, 'bin', 'behave')
    behave_path = os.path.normpath(behave_path)
    local('coverage run {add_arg} {behave_path}'.format(
                behave_path=behave_path, add_arg=add_arg))
    if os.path.exists(".coverage"):
        local("coverage html")