from distutils.core import setup
import pypirss

def make_dependencies():
    with open('pypirss.req') as fd:
        deps = fd.readlines()
    needed = ['Jinja2', 'pipe', 'web.py']
    return [dep.strip() 
            for dep in deps if dep.split('==')[0] in needed]

static_patterns = ['templates/*', 'static/*.png', 'static/*.js', 'static/*.css',
                   'static/*.gif', 'static/*-webfont.*']

setup(
  name='PypiRSS',
  version=pypirss.__version__,
  packages=['pypirss'],
  description='A simple RSS frontend for Pypi',
  author='Bruno Carlin',
  author_email='self@aerdhyl.eu',
  url='https://bitbucket.org/aerdhyl/pypirss',
  package_data={'pypirss': static_patterns},
  install_requires=make_dependencies(),
  license="MIT License"
)