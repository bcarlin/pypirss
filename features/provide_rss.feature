Feature: Provide rss
  In order to be informed about my favorite packages update
  As a user
  I want to get an rss feed for a package

  Scenario: Get a package rss feed
    When I fetch the rss feed for package 'roundup'
    Then the response has status 200
    And I get a valid rss feed
    
  Scenario: Get a non-existing package rss feed
    When I fetch the rss feed for package 'bluh'
    Then the response has status 404
  
  Scenario: Get a new feed when pypi is unavailable
    Given pypi is unavailable
    When I fetch the rss feed for package 'roundup'
    Then the response has status 504
  
  Scenario: Get a feed update when pypi is unavailable
    Given I already fetched the rss feed for package 'roundup'
    And pypi is unavailable
    When I fetch the rss feed for package 'roundup'
    Then the response has status 200
    And I get a valid rss feed
  
  
     
  
  
