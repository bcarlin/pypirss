Feature: Search
  In order to easily find the package I want
  As a user
  I want to search the list of packages for the package I want

  Scenario: search a non-existing package
    When I search for 'bluh'
    Then the response has status 200
    And response is of type 'application/json'
    And I get no result
  
  Scenario: search for a package with one result
    When I search for 'foobar'
    Then the response has status 200
    And response is of type 'application/json'
    And I get the result foobar
  
  Scenario: search for packages with several results
    When I search for 'blah'
    Then the response has status 200
    And response is of type 'application/json'
    And I get the result blah, blah-foo, blah_bar, quuzblah 
  
  Scenario: search for packages when pypi is unavailable
    Given I already fetched the list of packages
    And pypi is unavailable
    When I search for 'foobar'
    Then the response has status 200
    And response is of type 'application/json'
    And I get the result foobar
  
  Scenario: search for a package with an empty cache
    Given pypi is unavailable
    When I search for 'foobar'
    Then the response has status 504
  
  
  
  
  
  
