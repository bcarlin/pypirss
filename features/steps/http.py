from behave import then



@then(u'the response has status (?P<status_code>[1-6][0-9]{2})')
def the_response_has_status(ctx, status_code):
    status_code = int(status_code)
    assert ctx.res is not None
    assert ctx.res.status == status_code, \
        ("expected {error_code}, got {status} instead \n\n").format(
            error_code=status_code, status=ctx.res.status                                                                        
        ) + str(ctx.res)

@then(u"response is of type '(?P<content_type>[a-zA-Z\+\-\/]+)'")
def response_is_of_type(ctx, content_type):
    actual = ctx.res.header('Content-Type', None)
    assert actual == content_type, \
            "actual content-type: {actual}".format(actual=actual)