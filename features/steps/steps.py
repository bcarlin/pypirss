'''
Step definition for freshen tests
'''
#pylint: disable=W0613
#pylint: disable=C0111
#pylint: disable=C0103
#from freshen import Before, After, When, Then, glc, scc
from behave import given, when, then
from mock import Mock, patch
from pypirss.tests.mocks import pypi_proxy_error_mock
from utils import rss_url_for



    


@given('pypi is unavailable')
def pypi_is_unavailable(ctx):
    ctx.patcher = patch("pypirss.pypi.PypiProxy", 
                        Mock(return_value=pypi_proxy_error_mock))
    ctx.patched = ctx.patcher.start()

@given("I already fetched the rss feed for package '(?P<name>[a-zA-Z0-9_\-\.]+)'")
def i_already_fetched_the_rss_feed_for_package(ctx, name):
    i_fetch_the_rss_feed_for_package(ctx, name)

@when("I fetch the rss feed for package '(?P<name>[a-zA-Z0-9_\-\.]+)'")
def i_fetch_the_rss_feed_for_package(ctx, name):
    ctx.package_name = name
    ctx.res = ctx.app.get(rss_url_for(name), expect_errors=True)
    
    
@then('I get a valid rss feed')
def i_get_a_valid_rss_feed(ctx):
    assert ctx.res.header('Content-Type', None) == "application/rss+xml"
    ctx.res.mustcontain(ctx.package_name)

#@then("I get a (?P<error_code>[1-6][0-9]{2}) error")
#def i_get_a_404_error(ctx, error_code):
#    error_code = int(error_code)
#    assert ctx.res is not None
#    assert ctx.res.status == error_code, \
#        ("expected {error_code}, got {status} instead \n\n").format(
#            error_code=error_code, status=ctx.res.status                                                                        
#        )
