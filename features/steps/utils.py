

def rss_url_for(package):
    return "/package/{package}.rss".format(package=package)