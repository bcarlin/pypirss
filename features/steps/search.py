from behave import given, when, then
import nose.tools as test
import json
from pypirss.pypi import Pypi


@given(u'I already fetched the list of packages')
def i_already_fetched_the_list_of_packages(context):
    pypi = Pypi()
    pypi.get_package_list()

@when(u"I search for '(?P<search_string>[a-z]+)'")
def i_search_for(ctx, search_string):
    ctx.search_string = search_string
    ctx.res = ctx.app.post('/search/{search_string}'.format(
                                search_string=search_string),
                           expect_errors=True)

def parse_response(response):
    return json.loads(response) if response != "" else ""


@then(u'I get no result')
def I_get_no_result(ctx):
    response = parse_response(ctx.res.body)
    test.assert_list_equal([], response)


@then(u'I get the result (?P<result>[a-zA-Z0-9_\-,\. ]+)')
def I_get_the_result(ctx, result):
    result = [el.strip() for el in result.split(',')]
    response = parse_response(ctx.res.body)
    test.assert_set_equal(set(result), set(response))

