from pypirss.application import app
from pypirss.tests.mocks import pypi_proxy_mock
from mock import patch, Mock
from paste.fixture import TestApp
import sys
from pypirss.lica import SqliteBackend, DictBackend
from collections import defaultdict
import shutil

from behave.matchers import step_matcher
step_matcher('re')

def before_feature(ctx, fea):
    ctx.app = TestApp(app.wsgifunc()) 

def before_scenario(ctx, sce):
    ctx.patcher = patch("pypirss.pypi.PypiProxy", 
                        Mock(return_value=pypi_proxy_mock))
    ctx.patched = ctx.patcher.start()
        

def after_scenario(ctx, sce):
    ctx.patcher.stop()
    ctx.patched.reset_mock()
    ctx.app.reset()
    for k, db in SqliteBackend._dbs.items():
        db.close()
        del SqliteBackend._dbs[k]
    SqliteBackend._dbs = {}
    SqliteBackend._collections = {}
    shutil.rmtree(SqliteBackend.cache_path, True)
    DictBackend._data = defaultdict(defaultdict)