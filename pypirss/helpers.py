from datetime import datetime
from jinja2.exceptions import TemplateNotFound
import pipe
import urllib2
import json
from xml.sax.saxutils import escape
import re
import os
from subprocess import Popen

pipe.map = pipe.select


def convert_date(date):
    date = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
    date = date.strftime("%a, %d %b %Y %H:%M:%S %z").strip()
    return "{0} UT".format(date)

def xml_safe(txt):
    return escape(txt)

class render_jinja:
    def __init__(self, *a, **kwargs):
        extensions = kwargs.pop('extensions', [])
        self.file_ext = kwargs.pop('file_ext', ['html'])
        _globals = kwargs.pop('globals', {})

        from jinja2 import Environment,FileSystemLoader
        self._lookup = Environment(loader=FileSystemLoader(*a, **kwargs), 
                                   extensions=extensions)
        self._lookup.filters['rss_date'] = convert_date
        self._lookup.filters['xml_safe'] = xml_safe
        self._lookup.globals.update(_globals)
        
    def _find_template(self, name):
        paths = ["{0}.{1}".format(name, ext) for ext in self.file_ext]

        for path in paths:
            try:
                return self._lookup.get_template(path)
            except TemplateNotFound:
                pass
        raise TemplateNotFound(name)

    def __getattr__(self, name):
        t = self._find_template(name)
        return t.render

def get_json(url):
    try:
        url_fd = urllib2.urlopen(url)
        content = url_fd.read()
        return json.loads(content)
    finally:
        url_fd.close()

script_string = '<script type="{type}" src="{src}"></script>'
script_rx = re.compile(script_string.format(
    src='(?P<src>[a-zA-Z\-_\./]+\.coffee)',
    type='text/coffeescript'
))

def compile_coffeescript(src, dst):
    cmd = ["coffee", "-c", "-o", dst, src]
    Popen(cmd).wait()

def process_coffeescript(handler):
    result = handler()
    matches = script_rx.findall(result)
    for match in matches:
        src = match.strip('/')
        dst = os.path.dirname(src)
        compile_coffeescript(src, dst)
        result = result.replace(
            script_string.format(src=match, type='text/coffeescript'),
            script_string.format(
                src=match.replace('.coffee', '.js'),
                type="text/javascript",
            )
        )
    return result
        
    