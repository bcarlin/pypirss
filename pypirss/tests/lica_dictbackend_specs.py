# Warnings on protected members access :
# pylint: disable=W0212
# Missing docstring :
# pylint: disable=C0111
# Too many public methods:
# pylint: disable=R0904
# Method names too long: 
# pylint: disable=C0103
import unittest
from pypirss.lica import DictBackend
from collections import defaultdict
from pypirss.tests.lica_backend_suite_mixin import BackendSuiteMixin


class DescribeDictBackend(BackendSuiteMixin, unittest.TestCase):

    def setUp(self):
        self.klass = DictBackend
        self.base_args = {}

    def tearDown(self):
        DictBackend._data = defaultdict(defaultdict)
    
    def expires(self, name, key, new_date):
        DictBackend._data[name][key]['expires'] = new_date
    
    def get_exp_date(self, name, key):
        return DictBackend._data[name][key]['expires']

        
    def ensure_two_instances_share_the_same_data(self):
        cache1 = DictBackend()
        cache2 = DictBackend()
        
        cache1.put('foo', 'key', 'my data')
        self.assertEquals(cache2.get('foo', 'key'), 'my data')
        
        cache2.put('foo', 'key', 'my data2')
        self.assertEquals(cache1.get('foo', 'key'), 'my data2')
    



