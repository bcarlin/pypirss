
# pylint: disable=C0103
# pylint: disable=C0111
# pylint: disable=R0904
import unittest
from pypirss.pypi import Pypi, Package, Version, DataUnavailable
from mock import patch, Mock
from mocks import pypi_proxy_mock, pypi_proxy_error_mock
import shutil
from pypirss.lica import SqliteBackend, DictBackend
from collections import defaultdict
from datetime import datetime as dt, timedelta as td

class DescribePypi(unittest.TestCase): 

    def setUp(self): 
        pypi_proxy_mock.reset_mock()

    def tearDown(self):
        for k, db in SqliteBackend._dbs.items():
            db.close()
            del SqliteBackend._dbs[k]
        SqliteBackend._dbs = {}
        SqliteBackend._collections = {}
        shutil.rmtree(SqliteBackend.cache_path, True)
        DictBackend._data = defaultdict(defaultdict)

    def it_is_a_singleton(self):
        inst1 = Pypi()
        inst2 = Pypi()
        self.assertIs(inst1, inst2, "Pypi is not a singleton")
    
    def it_should_have_a_package_list(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            self.assertTrue(type(pypi.get_package_list()) is list)
    
    def it_should_cache_the_list_of_package(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            pypi.get_package_list()
            pypi.get_package_list()
            self.assertEquals(1, pypi_proxy_mock.get_package_list.call_count)
    
    def it_sould_verify_if_a_package_exists(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            self.assertTrue(pypi.package_exists('roundup'))
            self.assertFalse(pypi.package_exists('not-existing.package'))
        
    def it_should_return_none_if_a_package_does_not_exist(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            p = pypi.get_package("not-existing.package")
            self.assertIsNone(p)
    
    def it_should_return_a_package_if_it_exists(self):
        
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)), \
             patch("pypirss.pypi.Package", Mock(return_value=Mock(spec=Package))) as mockp:
            pypi = Pypi()
            p = pypi.get_package("roundup")
            self.assertIsInstance(p, Package)
    
    def it_provides_a_list_of_versions_for_a_given_package(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            p = pypi.get_versions("roundup")
            self.assertTrue(all(isinstance(item, Version) for item in p),
                            "at least one item was not a Version instance")
    
    def it_should_cache_the_list_of_versions_for_a_given_package(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            pypi.get_versions("roundup")
            pypi.get_versions("roundup")
            self.assertEqual(1, pypi_proxy_mock.get_versions.call_count)
    
    def it_provides_data_for_a_package_version(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            pypi.get_version_info("roundup", "1.4.19")
            self.assertNotEqual(0, pypi_proxy_mock.get_version_info.call_count)
    
    def it_should_cache_data_for_a_package_version(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            pypi.get_version_info("roundup", "1.4.19")
            pypi.get_version_info("roundup", "1.4.19")
            self.assertEqual(1, pypi_proxy_mock.get_version_info.call_count)
    
    def ensure_get_version_returns_at_most_ten_items(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            versions = pypi.get_versions("roundup")
            self.assertEquals(10, len(versions))
    
    def ensure_package_exists_raise_an_error_if_cache_is_empty_and_pypi_unavailable(self):
        with patch("pypirss.pypi.PypiProxy", 
                   Mock(return_value=pypi_proxy_error_mock)):
            pypi = Pypi()
            self.assertRaises(DataUnavailable, 
                              pypi.package_exists, 'foobar-baz')
    
    def ensure_get_version_info_raise_an_error_if_cache_is_empty_and_pypi_unavailable(self):
        with patch("pypirss.pypi.PypiProxy", 
                   Mock(return_value=pypi_proxy_error_mock)):
            pypi = Pypi()
            self.assertRaises(DataUnavailable, 
                              pypi.get_version_info, 'foobar-baz', '1.0')
    
    def ensure_get_versions_raise_an_error_if_cache_is_empty_and_pypi_unavailable(self):
        with patch("pypirss.pypi.PypiProxy", 
                   Mock(return_value=pypi_proxy_error_mock)):
            pypi = Pypi()
            self.assertRaises(DataUnavailable, 
                              pypi.get_versions, 'foobar-baz')
    
    def ensure_package_exists_returns_an_expired_value_if_pypi_unavailable(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            self.assertTrue(pypi.package_exists('roundup'))
        
        pypi.package_list._backend._data['package list'][('full_list',)]['expires'] = dt.utcnow() - td(days=1)
        self.assertIsNone(pypi.package_list.get('full_list'))
        
        with patch("pypirss.pypi.PypiProxy", 
                   Mock(return_value=pypi_proxy_error_mock)):
            pypi = Pypi()
            self.assertTrue(pypi.package_exists('roundup'))
    
    def ensure_get_version_info_cache_do_not_expire(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            record = pypi.get_version_info('roundup', '1.4.19')
            self.assertIsNotNone(record)
        
        query = "UPDATE {table} SET expires=? WHERE key=?"
        query = query.format(table='version_infos')
        
        db = SqliteBackend._dbs['cache/pypirss.db']
        db.execute(query, (dt.utcnow() - td(hours=12), "('roundup', '1.4.19')"))
        db.commit()
        exp_date = db.execute("select expires from version_infos ").fetchone()[0]
        self.assertLess(exp_date, dt.utcnow().strftime("%Y-%m-%d %H:%M:%S."),
                        "expiration date has not been updated")
        self.assertEquals(record, pypi.version_info.get('roundup', '1.4.19'),
                          "it is considered expired")

        with patch("pypirss.pypi.PypiProxy", 
                   Mock(return_value=pypi_proxy_error_mock)):
            pypi = Pypi()
            self.assertEquals(record, pypi.get_version_info('roundup', '1.4.19'), 
                              "it is considered expired")
        self.assertEqual(0, pypi_proxy_error_mock.get_version_info.call_count)
        self.assertEqual(1, pypi_proxy_mock.get_version_info.call_count)
    
    def ensure_get_versions_returns_an_expired_value_if_pypi_unavailable(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            pypi = Pypi()
            res = pypi.get_versions('roundup')
            self.assertIsNotNone(res)

        query = "UPDATE {table} SET expires=? WHERE key=?"
        query = query.format(table='package_versions')
        db = SqliteBackend._dbs['cache/pypirss.db']
        db.execute(query, (dt.utcnow() - td(days=1), "('roundup',)"))
        db.commit()
        exp_date = db.execute("select expires from package_versions WHERE key=\"('roundup',)\"").fetchone()[0]
        self.assertLess(exp_date, dt.utcnow().strftime("%Y-%m-%d %H:%M:%S."),
                        "expiration date has not been updated")
        
        self.assertIsNone(pypi.package_versions.get('roundup', '1.4.19'),
                          "it is not considered expired")
        
        with patch("pypirss.pypi.PypiProxy", 
                   Mock(return_value=pypi_proxy_error_mock)):
            pypi = Pypi()
            are_all_versions = all(isinstance(el, Version)
                                   for el in pypi.get_versions('roundup'))
            self.assertTrue(are_all_versions)
        self.assertEqual(1, pypi_proxy_mock.get_versions.call_count)

        
        
        
