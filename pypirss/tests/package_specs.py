'''
Unittests for pypirss.pypi.Package
'''
# pylint: disable=C0103
# pylint: disable=C0111
# pylint: disable=R0904
import unittest
from pypirss.pypi import Package, Pypi, Version
from mock import patch, Mock
from pypirss.tests.mocks import pypi_proxy_mock
from pypirss.lica import SqliteBackend, DictBackend
from collections import defaultdict
import shutil



class DescribePackage(unittest.TestCase): 


    def setUp(self):  
        pypi_proxy_mock.reset_mock()
    
    def tearDown(self):
        pypi_proxy_mock.reset_mock() 
        for k, db in SqliteBackend._dbs.items():
            db.close()
            del SqliteBackend._dbs[k]
        SqliteBackend._dbs = {}
        SqliteBackend._collections = {}
        shutil.rmtree(SqliteBackend.cache_path, True)
        DictBackend._data = defaultdict(defaultdict)
        

    def it_should_get_a_list_of_versions(self):
        with patch("pypirss.pypi.Pypi", Mock(return_value=Mock(Pypi))) as pypi:
            p = Package('roundup')
            self.assertEquals(1, p._pypi.get_versions.call_count)
            p._pypi.get_versions.assert_called_with("roundup")
    
    def it_should_have_a_versions_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Package('Django')
            self.assertIs(type(p.versions), list)
            self.assertTrue(all(map(lambda el: isinstance(el, Version), p.versions)))
    
    def it_should_have_a_name_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Package('Django')
            self.assertEquals(p.name, 'Django')
    
    def it_should_have_a_url_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Package('Django')
            self.assertEquals(p.url, 
                              'http://pypi.python.org/pypi/Django')
    
    def it_should_have_a_upload_time_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Package('Django')
            self.assertEquals(p.upload_time, '2011-07-15T17:58:54')
    
    def ensure_it_loads_even_with_no_version(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Package('foobar')
            self.assertListEqual([], p._versions)
             

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()