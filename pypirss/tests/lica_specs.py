import unittest
from pypirss.lica import Lica, DictBackend
from mock import patch, MagicMock, Mock


class DescribeLica(unittest.TestCase): 

        

    def it_can_store_values(self):
        cache = Lica('cache1')
        with patch.object(cache, "_backend") as mock_backend:
            cache.put('key1', "my data")
            mock_backend.put.assert_called_once_with('cache1', ('key1',), 
                                                     "my data")
            
    
    def it_can_store_values_with_a_two_part_key(self):
        cache = Lica('cache1')
        with patch.object(cache, "_backend") as mock_backend:
            cache.put('key1', 'key2', "my data")
            mock_backend.put.assert_called_once_with('cache1', ('key1', 'key2'), 
                                                     "my data")
    
    def it_can_retrieve_values(self):
        cache = Lica('cache1')
        with patch.object(cache, "_backend") as mock_backend:
            cache.get('key1')
            mock_backend.get.assert_called_once_with('cache1', ('key1',),
                                                     fallback=False)
    
    def it_can_retrieve_values_with_a_two_part_key(self):
        cache = Lica('cache1')
        with patch.object(cache, "_backend") as mock_backend:
            cache.get('key1', 'key2')
            mock_backend.get.assert_called_once_with('cache1',
                                                     ('key1', 'key2'),
                                                     fallback=False)
    
    def it_can_tell_if_it_has_a_key(self):
        cache = Lica('cache1')
        with patch.object(cache, "_backend", MagicMock()) as mock_backend:
            'key1' in cache
            mock_backend.contains.assert_called_once_with('cache1',
                                                          ('key1',))
            
    def it_can_tell_if_it_has_a_key_as_tuple(self):
        cache = Lica('cache1')
        with patch.object(cache, "_backend", MagicMock()) as mock_backend:
            ('key1',) in cache
            mock_backend.contains.assert_called_once_with('cache1',
                                                          ('key1',))
            
    def it_can_tell_if_it_has_a_two_part_key(self):
        cache = Lica('cache1')
        with patch.object(cache, "_backend", MagicMock()) as mock_backend:
            ('key1', 'key2') in cache
            mock_backend.contains.assert_called_once_with('cache1',
                                                          ('key1', 'key2'))
    
    def ensure_it_initialize_a_backend_at_init_time(self):
        cache = Lica("cache1")
        self.assertIsInstance(cache._backend, DictBackend)
        
    def ensure_it_passes_any_unknown_keyword_argument_to_the_backend(self):
        mock_backend = Mock()
        cache = Lica("cache1", backend=mock_backend, foo='bar')
        self.assertNotIn("backend", mock_backend.call_args[1])
        self.assertIn('foo', mock_backend.call_args[1])
    
    def it_can_fallback_on_an_expired_value_if_user_asks_for_it(self):
        mock_backend = Mock()
        cache = Lica('cache1')
        with patch.object(cache, "_backend") as mock_backend:
            cache.get('key1', fallback=True)
            mock_backend.get.assert_called_once_with('cache1',
                                                     ('key1',), fallback=True)
        

