'''
Unittests for pypirss.pypi.Version
'''
# pylint: disable=C0103
# pylint: disable=C0111
# pylint: disable=R0904
import unittest
from pypirss.pypi import Version, Pypi
from mock import Mock, patch
from pypirss.tests.mocks import pypi_proxy_mock
from pypirss.lica import SqliteBackend, DictBackend
import shutil
from collections import defaultdict


class DescribeVersion(unittest.TestCase): 




    def tearDown(self):
        pypi_proxy_mock.reset_mock() 
        for k, db in SqliteBackend._dbs.items():
            db.close()
            del SqliteBackend._dbs[k]
        SqliteBackend._dbs = {}
        SqliteBackend._collections = {}
        shutil.rmtree(SqliteBackend.cache_path, True)
        DictBackend._data = defaultdict(defaultdict)

    def it_should_get_version_data(self):
        with patch("pypirss.pypi.Pypi", Mock(return_value=Mock(Pypi))):
            p = Version('roundup', '1.4.19')
            self.assertEquals(1, p._pypi.get_version_info.call_count)
            p._pypi.get_version_info.assert_called_with("roundup", '1.4.19')
    
    def it_should_have_a_name_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Version('Django', '1.3.1')
            self.assertEquals(p.name, 'Django')
    
    def it_should_have_a_version_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Version('Django', '1.3.1')
            self.assertEquals(p.version, '1.3.1')
    
    def it_should_have_a_release_url_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Version('Django', '1.3.1')
            self.assertEquals(p.release_url, 
                              'http://pypi.python.org/pypi/Django/1.3.1')
    
    def it_should_have_an_upload_time__field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Version('Django', '1.3.1')
            self.assertEquals(p.upload_time, 
                              '2011-07-15T17:58:54')
    
    def it_should_have_an_author_email_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Version('Django', '1.3.1')
            self.assertEquals(p.author_email, 
                              'richard@users.sourceforge.net')
    
    def it_should_have_a_author_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Version('Django', '1.3.1')
            self.assertEquals(p.author, 
                              'Richard Jones')
    
    def it_should_have_a_description_field(self):
        with patch("pypirss.pypi.PypiProxy", Mock(return_value=pypi_proxy_mock)):
            p = Version('Django', '1.3.1')
            desc = """I'm proud to release 
                  version 1.4.19 of Roundup which introduces some minor features
                  and, as usual, fixes some bugs."""
            self.assertEquals(p.description, 
                              desc.replace("\n", ''))





if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()