"Unittests for the PypiProxy class"
# pylint: disable=C0103
# pylint: disable=C0111
# pylint: disable=R0904
import unittest
from pypirss.pypi import PypiProxy
from mock import patch, Mock
from mocks import xmlrpc_iface_mock, xmlrpc_iface_error_mock
from pypirss.errors import DataUnavailable
from urllib2 import URLError



class DescribePypiProxy(unittest.TestCase): 

    def setUp(self): 
        pass


    def tearDown(self):
        xmlrpc_iface_mock.reset_mock()


    def it_is_a_singleton(self):
        inst1 = PypiProxy()
        inst2 = PypiProxy()
        self.assertIs(inst1, inst2, "PypiProxy is not a singleton")
    
    @unittest.skip("network call")
    def it_should_fetch_a_list_of_packages_from_pypi(self):
        pypi_proxy = PypiProxy()
        with patch.object(PypiProxy(), "xmlrpc_iface", xmlrpc_iface_mock):
            package_list = pypi_proxy.get_package_list()
            self.assertIs(type(package_list), list)
            self.assertTrue(len(package_list) > 0)
    
    @unittest.skip("network call")        
    def it_should_fetch_a_list_of_version_for_a_package_from_pypi(self): 
        pypi_proxy = PypiProxy()
        with patch.object(PypiProxy(), "xmlrpc_iface", xmlrpc_iface_mock):
            pass
    @unittest.skip("network call")  
    def it_should_get_version_info_from_pypi(self):
        pass
    
    def ensure_get_versions_in_raises_UnavailableData_if_pypi_is_unavailable(self):
        pypi_proxy = PypiProxy()
        with patch.object(PypiProxy(), "xmlrpc_iface", xmlrpc_iface_error_mock):
            self.assertRaises(DataUnavailable, pypi_proxy.get_versions, 'foo')
    
    def ensure_get_version_info_in_raises_UnavailableData_if_pypi_is_unavailable(self):
        get_json_mock = Mock()
        get_json_mock.side_effect = URLError('foobar')
        with patch("pypirss.pypi.get_json", get_json_mock):
            pypi_proxy = PypiProxy()
            self.assertRaises(DataUnavailable, pypi_proxy.get_version_info, 
                              'foo', '1.0')

    def ensure_get_package_list_in_raises_UnavailableData_if_pypi_is_unavailable(self):
        pypi_proxy = PypiProxy()
        with patch.object(PypiProxy(), "xmlrpc_iface", xmlrpc_iface_error_mock):
            self.assertRaises(DataUnavailable, pypi_proxy.get_package_list)
    
        
            