# Warnings on protected members access :
# pylint: disable=W0212
# Missing docstring :
# pylint: disable=C0111
# Too many public methods:
# pylint: disable=R0904
# Method names too long: 
# pylint: disable=C0103
import unittest
from pypirss.lica import SqliteBackend
from datetime import datetime as dt
import os
import shutil
from pypirss.tests.lica_backend_suite_mixin import BackendSuiteMixin
import nose
import sys


class DescribeSqliteBackend(BackendSuiteMixin, unittest.TestCase): 

    def setUp(self):
        self.klass = SqliteBackend
        self.base_args = {'path': ":memory:"}

    def tearDown(self):
        shutil.rmtree(SqliteBackend.cache_path, True)
        for k, db in SqliteBackend._dbs.items():
            db.close()
            del SqliteBackend._dbs[k]
        SqliteBackend._dbs = {}
        SqliteBackend._collections = {}
        
    def expires(self, name, key, new_date):
        query = "UPDATE {table} SET expires=? WHERE key=?".format(table=name)
        SqliteBackend._dbs[':memory:'].execute(query, (new_date, 
                                                       repr(key)))
        SqliteBackend._dbs[':memory:'].commit()

    def get_exp_date(self, name, key):
        query = "SELECT expires FROM foo WHERE key=?"
        res_key = SqliteBackend._dbs[':memory:'].execute(query, (repr('key'),)).fetchone()[0]
        res_key = dt.strptime(res_key, '%Y-%m-%d %H:%M:%S.%f')
        return res_key

    

    def ensure_it_tries_to_create_its_own_path_if_it_does_fot_exists(self):
        cache = SqliteBackend(path="cache/foo/bar/baz/quz")
        cache.put('foo', 'key', 'data')
        self.assertTrue(os.path.exists(os.path.join('cache/foo/bar/baz/quz')))


    def ensure_a_cache_can_be_reopened(self):
        cache = SqliteBackend("cache/foo/bar/baz/quz")
        cache.put('foo', 'key', 'my data')
        
        for k, db in SqliteBackend._dbs.items():
            db.close()
            del SqliteBackend._dbs[k]
        SqliteBackend._dbs = {}
        SqliteBackend._collections = {}
        
        cache2 = SqliteBackend("cache/foo/bar/baz/quz")
        res = cache2.get('foo', 'key')
        self.assertEqual(res, 'my data')
        
    
    def ensure_two_instances_share_the_same_db(self):
        cache1 = SqliteBackend()
        cache2 = SqliteBackend()
        
        cache1.put('foo', 'key', 'my data')
        self.assertEquals(cache2.get('foo', 'key'), 'my data')
        
        cache2.put('foo', 'key', 'my data2')
        self.assertEquals(cache1.get('foo', 'key'), 'my data2')




