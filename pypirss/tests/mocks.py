from mock import Mock
from pypirss.pypi import PypiProxy
import json
import sys
from xmlrpclib import ProtocolError
from urllib2 import URLError
from pypirss.errors import DataUnavailable




version_info = {
    "roundup": {
        "1.4.19": 
                  """{"info":{"maintainer":"","requires_python":null, 
                  "maintainer_email":"","cheesecake_code_kwalitee_id": null, 
                  "keywords": "","package_url":
                  "http://pypi.python.org/pypi/roundup","author": 
                  "Richard Jones","author_email": 
                  "richard@users.sourceforge.net","download_url": 
                  "http://pypi.python.org/pypi/roundup","platform": "UNKNOWN",
                  "version": "1.4.19","cheesecake_documentation_id": null,
                  "_pypi_hidden": false,"description": "I'm proud to release 
                  version 1.4.19 of Roundup which introduces some minor features
                  and, as usual, fixes some bugs.","release_url":
                  "http://pypi.python.org/pypi/roundup/1.4.19","_pypi_ordering":
                  155,"classifiers":[
                  "Development Status :: 5 - Production/Stable", 
                  "Environment :: Console","Environment :: Web Environment", 
                  "Intended Audience :: Developers",
                  "Intended Audience :: End Users/Desktop", 
                  "Intended Audience :: System Administrators", 
                  "License :: OSI Approved :: Python Software Foundation 
                  License", "Operating System :: MacOS :: MacOS X", 
                  "Operating System :: Microsoft :: Windows", 
                  "Operating System :: POSIX", "Programming Language :: Python", 
                  "Topic :: Communications :: Email","Topic :: Office/Business", 
                  "Topic :: Software Development :: Bug Tracking"], 
                  "bugtrack_url": "http://issues.roundup-tracker.org/","name": 
                  "roundup", "license": "UNKNOWN", "summary": "A simple-to-use 
                  and -install issue-tracking system with command-line, web and 
                  e-mail interfaces. Highly customisable.","home_page": 
                  "http://www.roundup-tracker.org","stable_version": null,
                  "cheesecake_installability_id": null},"urls":[{"has_sig": 
                  true,"upload_time": "2011-07-15T17:58:54","comment_text": "", 
                  "python_version": "source","url": 
                  "http://pypi.python.org/packages/source/r/roundup/roundup-1.4.19.tar.gz",
                  "md5_digest": "70ea4373da67e7b18a21550266f41a90","downloads": 
                  1417, "filename": "roundup-1.4.19.tar.gz","packagetype": 
                  "sdist","size": 2505527},{"has_sig": true,"upload_time": 
                  "2011-07-15T17:59:32","comment_text":"","python_version":
                  "any","url": 
                  "http://pypi.python.org/packages/any/r/roundup/roundup-1.4.19.win32.exe", 
                  "md5_digest": "0a33f599532d499b7dc54887cb4db102","downloads": 
                  919,"filename": "roundup-1.4.19.win32.exe","packagetype": 
                  "bdist_wininst","size": 1145082}]}"""
    }
}

def _get_version_info_mock(package_name, version):
    #print >>sys.stderr, "_get_version_info_mockcalled"
    return json.loads(version_info["roundup"]['1.4.19'].replace('\n', ''))

def _package_releases_mock(package_name):
    #print >>sys.stderr, "_package_releases_mock called"
    versions = {"Django": ['1.3.1', '1.3', '1.2.7', '1.2.6', '1.2.5', 
                           '1.2.4', '1.2.3', '1.2.2', '1.2.1', '1.2', 
                           '1.1.4', '1.1.3', '1.1.2', '1.1.1', '1.1', 
                           '1.0.4', '1.0.3', '1.0.2', '1.0.1', '1.0'],
                "roundup": ['1.4.19', '1.4.18', '1.4.17', '1.4.16', 
                            '1.4.15', '1.4.14', '1.4.13', '1.4.12', 
                            '1.4.11', '1.4.10', '1.4.9', '1.4.8', 
#                            '1.4.7', '1.4.6', '1.4.5.1', '1.4.4', 
#                            '1.4.3', '1.4.2', '1.4.1', '1.4.0', '1.3.3', 
#                            '1.3.2', '1.3.1', '1.3.0', '1.2.1', '1.2.0', 
#                            '1.1.2', '1.1.1', '1.1.0', '1.0.1', '1.0', 
#                            '0.9.0b1', '0.8.6', '0.8.5', '0.8.4', 
#                            '0.8.3', '0.8.2', '0.8.1', '0.8.0b1', 
#                            '0.8.0', '0.7.12', '0.7.11', '0.7.9', 
#                            '0.7.8', '0.7.7', '0.7.5', '0.7.4', '0.7.3', 
#                            '0.7.2', '0.7.1', '0.7.0b3', '0.7.0', 
                            '0.6.11', '0.6.9', '0.6.8', '0.5.9']}
    return versions.get(package_name, None)

mock_package_list = ['roundup', 'Django', 'foobar', 
                     'blah', 'blah-foo', 'blah_bar', 'quuzblah']


xmlrpc_iface_mock = Mock()
xmlrpc_iface_mock.list_packages.return_value = mock_package_list
xmlrpc_iface_mock.package_releases.side_effect = _package_releases_mock

xmlrpc_iface_error_mock = Mock()
xmlrpc_iface_error_mock.list_packages.side_effect = ProtocolError("", 1, "", "")
xmlrpc_iface_error_mock.package_releases.side_effect = ProtocolError("", 1, "", "")

pypi_proxy_mock = Mock(spec=PypiProxy)
pypi_proxy_mock.get_version_info.side_effect = _get_version_info_mock
pypi_proxy_mock.get_versions.side_effect = _package_releases_mock
pypi_proxy_mock.get_package_list.return_value = mock_package_list
pypi_proxy_mock.xmlrpc_iface = xmlrpc_iface_mock

pypi_proxy_error_mock = Mock(spec=PypiProxy)
pypi_proxy_error_mock.get_version_info.side_effect = DataUnavailable()
pypi_proxy_error_mock.get_versions.side_effect = DataUnavailable()
pypi_proxy_error_mock.get_package_list.side_effect = DataUnavailable()
