'''
Unittests for pypirss.tests.lica_backend_base
'''
from datetime import datetime as dt, timedelta as td


class BackendSuiteMixin(): 


    def make_args(self, **kw):
        tmp = self.base_args.copy()
        tmp.update(kw)
        return tmp
    
    def get_inst(self, **kw):
        return self.klass(**self.make_args(**kw))

    def ensure_an_error_is_raised_when_the_user_does_not_give_a_timedelta_as_validity(self):
        self.assertRaises(TypeError, self.klass, **self.make_args(timespan=3))

    
    def it_deactivates_expiration_if_timespan_equals_zero(self):
        cache = self.get_inst(timespan=0)
        cache.put('foo', 'key', 'my data')
        cache.put('foo', 'key2', 'my data 2')
        
        self.expires('foo', 'key', dt.utcnow() - td(days=1))
        
        self.assertFalse(cache.has_expired('foo', 'key'))
        self.assertFalse(cache.has_expired('foo', 'key2'))

    def ensure_the_user_can_set_the_validity_period_at_init_time(self):
        cache = self.get_inst(timespan=td(hours=3))
        cache.put('foo', 'key', 'my data')
        
        exp_date = self.get_exp_date('foo', 'key')
        
        self.assertTrue(exp_date < dt.utcnow() + td(hours=4))
        self.assertTrue(exp_date > dt.utcnow() + td(hours=2))
    
    def it_can_store_values(self):
        cache = self.get_inst()
        cache.put('foo', 'key1', "my_data")
        self.assertTrue(cache.contains('foo', 'key1'))
    
    def it_can_store_values_with_a_two_part_key(self):
        cache = self.get_inst()
        cache.put('foo', ('key1', 'key2'), "my_data")
        self.assertTrue(cache.contains('foo', ('key1', 'key2')))
    
    
    def it_can_retrieve_values(self):
        cache = self.get_inst()
        cache.put('foo', 'key', 'my data')
        self.assertEquals(cache.get('foo', 'key'), 'my data')
        
    def it_can_retrieve_values_with_a_two_part_key(self):
        cache = self.get_inst()
        cache.put('foo', ('key1', 'key2'), 'my data')
        self.assertEquals(cache.get('foo', ('key1', 'key2')), 'my data')
    
    
    def ensure_a_value_can_be_overwritten(self):
        cache = self.get_inst()
        cache.put('foo', 'key', 'my data')
        cache.put('foo', 'key', 'my new data')
        self.assertEquals(cache.get('foo', 'key'), 'my new data')
    
    def ensure_a_value_can_be_overwritten_with_a_two_part_key(self):
        cache = self.get_inst()
        cache.put('foo', ('key1', 'key2'), 'my data')
        cache.put('foo', ('key1', 'key2'), 'my new data')
        self.assertEquals(cache.get('foo', ('key1', 'key2')), 'my new data')
    
    def it_can_tell_if_a_value_has_expired(self):
        cache = self.get_inst()
        cache.put('foo', 'key', 'my data')
        cache.put('foo', 'key2', 'my data 2')
        
        self.expires('foo', 'key', dt.utcnow() - td(days=1))
        
        self.assertTrue(cache.has_expired('foo', 'key'))
        self.assertFalse(cache.has_expired('foo', 'key2'))
    
    def ensure_an_expired_date_is_updated_when_a_value_is_updated(self):
        cache = self.get_inst()
        cache.put('foo', 'key', 'my data')
        
        self.expires('foo', 'key', dt.utcnow() - td(days=1))
        
        cache.put('foo', 'key', 'my data 2')
        
        self.assertFalse(cache.has_expired('foo', 'key'))
        self.assertEquals(cache.get('foo', 'key'), 'my data 2')
    
    def it_returns_none_if_an_expired_value_is_retrieved(self):
        cache = self.get_inst()
        cache.put('foo', 'key', 'my data')
        
        self.expires('foo', 'key', dt.utcnow() - td(days=1))

        self.assertTrue(cache.has_expired('foo', 'key'))
        self.assertIsNone(cache.get('foo', 'key'))
        
    def it_considers_a_non_existing_value_as_expired_when_checking(self):
        cache = self.get_inst()
        self.assertTrue(cache.has_expired('foo', 'key'))
        
    def it_returns_none_if_the_cache_does_not_exists(self):
        cache = self.get_inst()
        cache.put('foo', 'key', 'my data')
        self.assertIsNone(cache.get('bar', 'key'))
    
    def it_returns_none_if_a_value_does_not_exist_in_the_cache(self):
        cache = self.get_inst()
        cache.put('foo', 'key', 'my data')
        self.assertIsNone(cache.get('foo', 'key2'))
    
    def it_accepts_anything_as_value(self):
        test_cases = (
            'my data', ['my', 'data'], ['my', 'other', 'data'],
            42, ['foo', {1: 'bar', 'baz': 'quz'}]
        )
        for test_case in test_cases:
            self._check_resulting_value(test_case)
        
    def _check_resulting_value(self, test_case):
        cache = self.get_inst()
        cache.put('foo', 'key', test_case)
        self.assertEquals(cache.get('foo', 'key'), test_case)
    
    def it_can_fallback_on_an_expired_value_if_user_asks_for_it(self):
        cache = self.get_inst()
        cache.put('foo', 'key', 'my data')
        cache.put('foo', 'key 2', 'my data 2')
        
        self.expires('foo', 'key', dt.utcnow() - td(days=1))
        
        self.assertIsNone(cache.get('foo', 'key'))
        self.assertEquals(cache.get('foo', 'key', fallback=True),
                          ('my data', True))
        self.assertEquals(cache.get('foo', 'key 2', fallback=True),
                          ('my data 2', False))
        self.assertEquals(cache.get('foo', 'key3', fallback=True),
                          (None, True))
            