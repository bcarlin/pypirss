makeLink = (item) ->
  a = new Element 'a', 
    href: "/package/#{item}.rss"
  a.insert new Element "img", 
    src: "/static/rss.png"

makeLi = (item) ->
  new Element('li').insert(makeLink item).insert(item)


class SlidingPager
  constructor: (parent, @paged) ->
    $(parent).insert '<div id="pager"><div id="larrow"></div><div id="rarrow"></div><div class="body"><div id="marker"></div></div></div>', 'bottom'
    @root = $('pager')
    @marker = $('marker')
    @hide()
    @nbPages = 1
    @currentPage = 1
    @marks = []
    @trackWidth = @root.first('.body').dimensions().width
    @markerLength = @trackWidth
    @setListeners()
   
  hide: ->
    @root.setStyle visibility: "hidden"
  
  show: ->
    @updateMarker()
    @root.setStyle visibility: "visible"
   
  update: (nbPages) ->
    @nbPages = nbPages
    @currentPage = 1
    if @nbPages is 1
      @marks = []
      @hide()
      return
    @marks = (@trackWidth * i / @nbPages for i in [0...@nbPages])
    @currentPage = 1
    @markerLength = @trackWidth / @nbPages
    @marker.setWidth @markerLength
    @show()
  
  updateMarker: (page = @currentPage)->
    @marker.moveTo x: @marks[page-1], y: -10
    @marker.update page
  
  setListeners: ->
    $('larrow').on
      click: =>
        @currentPage = Math.max(@currentPage-1, 1)
        @updateMarker()
        @paged.prevPage()
    $('rarrow').on
      click: =>
        @currentPage = Math.min(@currentPage+1, @nbPages)
        @updateMarker()
        @paged.nextPage()
    
    @root.first('.body').on 
      mousemove: (event) =>
        if event.currentTarget isnt event.relatedTarget 
          offset = event.position().x - @root.first('.body').position().x
        else
          offset = event.offset().x
        mark = @marks.first((el)-> el >= offset)
        pageToShow = if mark? then @marks.indexOf(mark) else @nbPages
        @updateMarker pageToShow
      mouseout: (event) =>
        @updateMarker()
    
    @marker.on
      click: (event) =>
        @currentPage = event.currentTarget.html().toInt()
        @paged.gotoPage @currentPage

  
  
class SimplePager
  constructor: (parent, @paged) ->
    $(parent).insert '<div id="pager" class="pager-hidden"><ul class="body"></ul></div>'
    @root = $('pager').first('.body')
    @update 0
    @_setEvents()
  
  update: (nbPages) ->
    @nbPages = nbPages
    @currentPage = 1
    unless @nbPages > 1
      @_hide()
      return
    @root.update @_makeHtml()
    @_highlight()
    @_show()
  
  _makeRange: ->
    downLimit = Math.max 2, @currentPage - 3
    upLimit = Math.min @nbPages-1, @currentPage + 3
    res = [[1]]
    res.push ['...'] if downLimit > 2
    res.push [downLimit..upLimit]
    res.push ['...'] if upLimit < @nbPages - 2
    res.push [@nbPages]
    return res
        
  _highlight: (el=@currentPage) ->
    if typeof el is "number"
      el = @root.children('li').first((li)-> li.html() is "#{el}")
    @root.first('.pager-highlight')?.removeClass('pager-highlight')
    el.addClass('pager-highlight')
  
  _hide: ->
    @root.addClass 'pager-hidden'
  
  _show: ->
    @root.removeClass 'pager-hidden'
  
  _pagerElem: (val) -> 
    "<li class=\"pager-item\">#{val}</li>"
  
  _makeHtml: ->
    items = [['<'], @_makeRange(), ['>']].flatten()
    @root.setWidth items.length * 32
    items.map(@_pagerElem).join('')
  
  _setEvents: -> 
    @root.delegate 'click', 'li', (event) =>
      switch event.target.html()
        when "&lt;"
          @currentPage = Math.max(@currentPage-1, 1)
          @paged.prevPage()
        when "&gt;"
          @currentPage = Math.min(@currentPage+1, @nbPages)
          @paged.nextPage()
        when "..."
          first = @root.children('li').indexOf(event.target) is 2
          if first
            @currentPage = Math.max(@currentPage-5, 1)
          else 
            @currentPage = Math.min(@currentPage+5, @nbPages)
          @paged.gotoPage @currentPage
        else
          @currentPage = event.target.html().toInt()
          @paged.gotoPage @currentPage
          
      @root.update @_makeHtml()
      @_highlight()


class ResultList
  constructor: (parent) ->
    @root = new Element('ul').insertTo $(parent)
    @pager = new SimplePager parent, this
    @items = []
    @page = 1
    @perPage = if $$('html')[0].hasClass('csscolumns') then 20 else 10
  
  update: (new_items) =>
    @page = 1
    @items = new_items
    @_updateDisplay()
    @pager.update @_nbPages()
  
  _updateDisplay: ->
    @_displayItems @_itemsForPage()
    
  
  _itemsForPage: ->
    return new Element('li').insert('No result...') if @items.length is 0
    start = (@page - 1) * @perPage
    end = Math.min @items.length, @page * @perPage
    (makeLi el for el in @items[start...end])
  
  _nbPages: ->
    Math.ceil @items.length / @perPage or 1
  
  _displayItems: (items) ->
    @root.update(items)
    
  nextPage: ->
    @gotoPage ++@page
  
  prevPage: ->
    @gotoPage --@page
  
  gotoPage: (pageNb) ->
    pageNb = Math.max(pageNb, 1)
    pageNb = Math.min(pageNb, @_nbPages())
    @page = pageNb
    @_updateDisplay()
    

sendForm = (form, resultList) ->
  $('q').addClass "spinnerbg"
  (->_gaq.push ['_trackEvent', 'Search', 'sent', form._.q.value]).delay(250)
  form.send
    onComplete: ->
      $('q').removeClass "spinnerbg"
    onSuccess: (response) ->
      resultList.update response.json
    onFailure: (response) ->
      resultList.root.update new Element('li').insert("an error happend... sorry!")
    
$(document).on 'ready', ->
  input = $('q')
  form = $('search')
  base_url = form.get 'action'
  defered = null
  resultList = new ResultList 'result'
  
  form.on
    submit: ->
      return false
  
  input.on 'keyup', ->
    defered?.cancel()
    unless input.value().length < 3
      form.set 'action', "#{base_url}/#{input.value()}"
      defered = (-> sendForm form, resultList).delay(400)
      