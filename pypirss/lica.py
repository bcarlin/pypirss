from datetime import timedelta as td, datetime as dt
from collections import defaultdict
import os
import sqlite3
import pickle

class BaseBackend(object):
    def __init__(self, timespan=td(hours=1)):
        """
        :param timedelta timespan: the period during which a value
                                   is considered valid
        """ 
        if not (isinstance(timespan, td) or timespan == 0):
            raise TypeError("timespan argument must be a "
                            "datetime.timedelta object")
        self._timespan = timespan
    
    def has_expired(self, name, key):
        """Checks whether the value identified by :param:`key` has expired or
        not. If there is no such value, returns ``False``"""
        return not self.contains(name, key) # pylint: disable=E1101

class DictBackend(BaseBackend):
    
    _data = defaultdict(defaultdict)
    
    def __init__(self, timespan=td(hours=1)):
        """
        :param timedelta timespan: the period during which a value
                                   is considered valid
        """ 
        BaseBackend.__init__(self, timespan)

    
    def put(self, name, key, value):
        """Store a :param:`value` in the cache identified by 
        :param:`key`. :param:`value` must be hashable"""
        expires = dt.utcnow() + self._timespan if self._timespan != 0 else 0
        self._data[name][key] = {'content': value,
                                 'expires': expires}
    
    def get(self, name, key, fallback=False):
        """Retrieve from the cache the value identified by :param:`key`
        and returns it. If there is no such value or if it has expired,
        ``None`` is returned."""
        outdated = True
        res = None
        if self.contains(name, key):
            outdated = False
            res = self._data[name][key]['content']
        elif fallback and key in self._data[name]:
            res = self._data[name][key]['content']
            
        return (res, outdated) if fallback else res
    
    def contains(self, name, key):
        return key in self._data[name] \
               and (self._timespan == 0 \
                    or dt.utcnow() < self._data[name][key]['expires'])
    

class SqliteBackend(BaseBackend):
    cache_path = 'cache'
    _dbs = {}
    _collections = {}
    
    def _setup(self, name):
        backend = self._open()
        query = "CREATE TABLE IF NOT EXISTS {table} "
        query += "(key text, value text, expires, UNIQUE (key))"
        backend.execute(query.format(table=name))
        return backend
    
    def _open(self):
        if self.path not in self._dbs:
            if not os.path.exists(self.path) and self.path != ':memory:':
                os.makedirs(os.path.dirname(self.path))
            self._dbs[self.path] = sqlite3.connect(self.path, 
                                                   check_same_thread = False)
            self._dbs[self.path].text_factory = str
            
            query = "SELECT name FROM sqlite_master "
            query += "WHERE type='table'"
            res = self._dbs[self.path].execute(query).fetchall()
            for (table,) in res:
                self._collections[table] = self._dbs[self.path]
        return self._dbs[self.path]
    
    def __init__(self, path=None, timespan=td(hours=1)):
        """
        :param str path: path to the SQLite file
        :param timedelta timespan: the period during which a value
                                   is considered valid
        """ 
        BaseBackend.__init__(self, timespan)
        self.path = path if path is not None else os.path.join(self.cache_path,
                                                               "default.db")
        self._open()
    
    def put(self, name, key, value):
        """Store a :param:`value` in the cache identified by 
        :param:`key`. :param:`value` must be hashable"""
        if name not in self._collections:
            self._collections[name] = self._setup(name)
        query = "INSERT OR REPLACE INTO {table} VALUES (?,?,?)"
        data = (repr(key), pickle.dumps(value), 
                dt.utcnow() + self._timespan if self._timespan != 0 else 0)
        self._collections[name].execute(query.format(table=name), data)
        self._collections[name].commit()
    
    def get(self, name, key, fallback=False):
        """Retrieve from the cache the value identified by :param:`key`
        and returns it. If there is no such value or if it has expired,
        ``None`` is returned."""
        if name not in self._collections: 
            return (None, True) if fallback else None
        value = None
        outdated = True
        
        query = "SELECT value, expires FROM {table} WHERE key=? LIMIT 1"
        query = query.format(table=name)
        res = self._collections[name].execute(query, (repr(key),)).fetchone()
        
        if res is not None:
            if res[1] > dt.utcnow().strftime("%Y-%m-%d %H:%M:%S.") \
                or self._timespan == 0:
                value = res[0]
                outdated = False
            else:
                if fallback:
                    value = res[0]
                
        if value is not None:        
            value = pickle.loads(value)
        return (value, outdated) if fallback else value
        
    
    def contains(self, name, key):
        res = self.get(name, key)
        return res is not None
    


class Lica(object):
    def __init__(self, name, **kw):
        self.name = name
        backend = kw.pop('backend', DictBackend)
        self._backend = backend(**kw)
    
    def put(self, *args):
        """Store a :param:`value` in the cache identified by 
        :param:`key`. :param:`value` must be hashable"""
        keys, value = args[:-1], args[-1]
        self._backend.put(self.name, keys, value)
    
    def get(self, *keys, **options):
        """Retrieve from the cache the value identified by :param:`key`
        and returns it. If there is no such value or if it has expired,
        ``None`` is returned."""
        fallback = options.pop('fallback', False)
        return self._backend.get(self.name, keys, fallback=fallback)
    
    def __contains__(self, keys):
        if type(keys) not in (tuple, list):
            keys = (keys,)
        return self._backend.contains(self.name, keys)






